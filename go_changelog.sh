#!/bin/sh

git log --graph --no-merges > CHANGELOG
pigz -11 -vv CHANGELOG
